# frozen_string_literal: true

CH_MESSAGE = <<~MSG
This merge request requires a ClickHouse review. To make sure these
changes are reviewed, take the following steps:

1. Ensure the merge request has ~clickhouse and ~"clickhouse::review pending" labels.
1. Assign and mention a ClickHouse reviewer.
MSG

CH_UNREVIEWED_LABEL = 'clickhouse::review pending'
CH_APPROVED_LABEL = 'clickhouse::approved'

return if stable_branch.valid_stable_branch?
return if helper.mr_labels.include?(CH_UNREVIEWED_LABEL)

if helper.mr_labels.include?('clickhouse') || clickhouse.changes.any?
  message 'This merge request adds or changes files that require a ' \
          'review from the [GitLab ClickHouse team](https://gitlab.com/groups/gl-clickhouse/-/group_members).'

  markdown(CH_MESSAGE)

  helper.labels_to_add << CH_UNREVIEWED_LABEL unless helper.has_scoped_label_with_scope?("clickhouse")
end
